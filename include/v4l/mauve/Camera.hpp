/****
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE V4L project.
 *
 * MAUVE V4L is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE V4L is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 ****/
#ifndef MAUVE_V4L_CAMERA_HPP
#define MAUVE_V4L_CAMERA_HPP

#include <mauve/runtime.hpp>
#include <mauve/base/PeriodicStateMachine.hpp>
#include <mauve/types/sensor/Image.hpp>
#include <opencv2/opencv.hpp>

namespace v4l {
  namespace mauve {

    /** Camera shell */
    struct CameraShell : public ::mauve::runtime::Shell {
      /** Image output port */
      ::mauve::runtime::WritePort<::mauve::types::sensor::Image>& image
        = mk_write_port<::mauve::types::sensor::Image>("image");
      /** Camera timestamp output port */
      ::mauve::runtime::WritePort<double>& time = mk_write_port<double>("time");
      /** Camera FPS property */
      ::mauve::runtime::Property<double>& fps = mk_property<double>("fps", 15.0);
      /** Camera ID property */
      ::mauve::runtime::Property<int>& id = mk_property<int>("id", -1);
    };

    /** Camera core */
    struct CameraCore : ::mauve::runtime::Core<CameraShell> {
      /** Grab the camera and publish */
      void update();
    protected:
      bool configure_hook() override;
      void cleanup_hook() override;
    private:
      cv::VideoCapture camera;
      ::mauve::types::sensor::Image image;
      bool open();
      bool configFPS();
    };

    /** Camera FSM: a Periodic State Machine */
    struct CameraFSM : public ::mauve::base::PeriodicStateMachine<CameraShell, CameraCore> {
    protected:
      bool configure_hook() override;
    };

    using Camera = ::mauve::runtime::Component<CameraShell, CameraCore, CameraFSM>;

  }
}

#endif // MAUVE_V4L_CAMERA_HPP
