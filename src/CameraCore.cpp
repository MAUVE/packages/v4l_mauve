/****
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE V4L project.
 *
 * MAUVE V4L is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE V4L is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 ****/
#include "v4l/mauve/Camera.hpp"

namespace v4l {
  namespace mauve {

bool CameraCore::open() {
  camera.open(shell().id);
  if (!camera.isOpened()) {
    logger().error("Unable to open Camera with id {}", shell().id.get());
    return false;
  }
  logger().info("Camera opened");
  return true;
}

bool CameraCore::configFPS() {
  bool r = camera.set(CV_CAP_PROP_FPS, shell().fps);
  if (r) {
    logger().info("Camera running at {} fps", shell().fps.get());
    return true;
  }
  else {
    logger().warn("Cannot set fps to {}", shell().fps.get());
    logger().warn("Camera FPS is {}", camera.get(CV_CAP_PROP_FPS));
    shell().fps.set(camera.get(CV_CAP_PROP_FPS));
    return false;
  }
}

bool CameraCore::configure_hook() {
  if (! open()) return false;
  image.image = new cv::Mat();
  configFPS();
  return true;
}

void CameraCore::cleanup_hook() {
  camera.release();
}

void CameraCore::update() {
  using namespace ::mauve::types::sensor;

  if ( camera.grab() ) {
    if ( camera.retrieve(* (image.image)) ) {
      double msec = camera.get(cv::CAP_PROP_POS_MSEC) * 0.001;
      logger().debug("image timestamp {:03.4f}", msec);
      shell().time.write(msec);
      // Get image
      switch ( image.image->type() ) {
        case CV_8UC3: image.encoding = image_encodings::BGR8; break;
        case CV_8UC1: image.encoding = image_encodings::MONO8; break;
      }
      image.width = image.image->cols;
      image.height = image.image->rows;
      logger().debug("got an image of size {}x{}", image.width, image.height);
      // Write image
      shell().image.write(image);
    }
  }
}

  }
}
